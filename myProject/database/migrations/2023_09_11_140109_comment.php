<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('commentblog', function (Blueprint $table) {
            $table->id('id_comment');
            $table->string('avatar');
            $table->string('name');
            $table->string('comment');
            $table->unsignedBigInteger('id_user'); 
            $table->unsignedBigInteger('id_blog'); 
            $table->integer('level');
            $table->timestamps(); 

            // Khóa ngoại đến bảng người dùng và bài viết
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_blog')->references('id_blog')->on('blog');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('commentblog');
    }
};
