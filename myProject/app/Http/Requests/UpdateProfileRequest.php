<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
         return [
        'phone' => 'nullable|string',
        'address' => 'nullable|string',
        'id_country' => 'nullable|integer',
        'avatar' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
    ];
    }
}
