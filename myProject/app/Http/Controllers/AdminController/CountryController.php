<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AdminModel\CountryModel;
use Illuminate\Support\Facades\Auth;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $dataCountry = CountryModel::paginate(4);
        // dd($dataCountry);
        return view('admin/country/country',compact('dataCountry'));
    }

    /**
     * Show the form for creating a new resource.
     */
     
    public function create(Request $request)
    {
        CountryModel::insert([
            'name'=>$request->name,
        ]);
        return redirect('country')->with('success', 'Thêm thành công!');

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $dataName = CountryModel::Where('id_country',$id)->get()->toArray();
        // dd($dataName);
        return view('admin/country/editCountry',compact('dataName'));  
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        CountryModel::Where('id_country',$id)->update([
            'name' => $request->name,
        ]);
        return redirect('country')->with('success', 'Edit thành công!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        CountryModel::Where('id_country',$id)->delete();
        return redirect('country')->with('success', 'Xoa thanh cong !');
    }
}
