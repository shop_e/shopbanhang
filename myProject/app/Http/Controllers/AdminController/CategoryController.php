<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AdminModel\CategoryModel;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $dataCategory = CategoryModel::all()->toArray();
        return view('admin/category/category',compact('dataCategory'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        CategoryModel::insert([
            'name_category'=>$request->name_category,
        ]);
        return redirect('category')->with('success', 'Thêm thành công!');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $editcategory = CategoryModel::where('id_category',$id)->get()->toArray();
        // dd($editcategory);
        $editcategory = $editcategory[0];
        return view('admin/category/edit_category',compact('editcategory'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        CategoryModel::where('id_category',$id)->update([
            'name_category'=>$request->name_category,
        ]);
        return redirect('category')->with('success', 'Edit thành công!');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        CategoryModel::where('id_category',$id)->delete();
        return redirect('category')->with('success', 'Xóa thành công!');

    }
}
