<?php

namespace App\Http\Controllers\MemberController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginCotroller extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function show()
    {
        return view('fontend/users/login');
    }
    public function login(Request $request)
    {
        $login = [
            'email'=>$request->email,
            'password'=>$request->password,
            'level'=>0,
        ];

        $remeber = false;

        if($request->remeber_me){
            $remeber = true;
        }
        if (Auth::attempt($login, $remeber)){
            return redirect('/home');
        }else{
            echo "Đăng nhập sai ";
        }

    }
     public function logout()
    {
        Auth::logout(); // Đăng xuất người dùng;
        return redirect('/home');
    }

}
