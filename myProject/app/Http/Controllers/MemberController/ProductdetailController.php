<?php

namespace App\Http\Controllers\MemberController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserModel\ProductsModel;
use App\Models\AdminModel\CategoryModel;
use App\Models\AdminModel\BrandModel;


class ProductdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(string $id)
    {   
        $dataProduct = ProductsModel::where('id_product',$id)->get()->toArray();
            $detail = $dataProduct[0];
        
        // dd($dataProduct);
        return view('fontend/product/product_detail',compact('detail'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function show()
    {
        $dataCategory = CategoryModel::all()->toArray();
        $dataBrand = BrandModel::all()->toArray();
        $data =ProductsModel::all()->toArray();
        return view('fontend/product/product',compact('data','dataCategory','dataBrand'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
