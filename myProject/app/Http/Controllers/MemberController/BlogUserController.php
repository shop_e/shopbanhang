<?php

namespace App\Http\Controllers\MemberController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AdminModel\BlogModel;
use App\Models\UserModel\rateModel;
use App\Models\UserModel\CommentModel;

use Illuminate\Support\Facades\Auth;

class BlogUserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show()
    {
        $dataBlog = BlogModel::paginate(3);
         return view('fontend/blog/blog',compact('dataBlog'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function showdetail(string $id)
    { 
        $dataCmt = CommentModel::where('id_blog',$id)->get()->toArray();
        // dd($dataCmt);
        $data = rateModel::all()->toArray();
        $rate=[];
        // dd($data);
        foreach ($data as $value) {
            $rate[]= $value['rate'];
        }
        $TBC = array_sum($rate)/count($rate);
       
            
        $dataDetail = BlogModel::where('id_blog',$id)->get()->toArray();
        $dataDetail = $dataDetail[0];
        return view('fontend/blog/blogdetail', compact('dataDetail',"TBC",'dataCmt' ));   
    }   

    /**
     * Update the specified resource in storage.
     */
    public function rateblog(Request $request)
    {
        $rate = $_POST['Values'];
        $id_blog = $_POST['id_blog'];
        $id_user = Auth::id();
        echo $rate;
        echo $id_blog;
        echo $id_user;
        rateModel::insert([
            'id_user'=>$id_user,
            'id_blog'=>$id_blog,
            'rate'=>$rate,
        ]);
    }
    public function cmtblog(Request $request){
        $id_blog = $request->id_blog;
        $id_user = Auth::user()->id;
        $name = Auth::user()->name;
        $avatar = Auth::user()->avatar;
        // dd($avatar);
        CommentModel::insert([
            'name'=>$name,
            'avatar'=>$avatar,
            'comment'=>$request->ndcomment,
            'id_user'=>$id_user,
            'id_blog'=>$id_blog,
            'level'=>$request->level, 
        ]);
        return redirect()->back()->with('success', 'Comment successfully.');
        
    }
    public function cmtnho(Request $id){
        $dataCon = CommentModel::all()->where("level",$id)->toArray();
        dd($dataCon);
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
