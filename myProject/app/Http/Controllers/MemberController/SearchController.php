<?php

namespace App\Http\Controllers\MemberController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserModel\ProductsModel;
use App\Models\AdminModel\CategoryModel;
use App\Models\AdminModel\BrandModel;
use App\Http\Requests\SearchProdutcRequest;
class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
       
        // dd($dataCategory);

        return view('fontend/search/search_product');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function gettext(SearchProdutcRequest $request)
    {   $dataBrand = BrandModel::all()->toArray();
        $dataCategory = CategoryModel::all()->toArray();
        $gettext = $request->textsearch;
        $data = ProductsModel::where('name', 'like', '%'.$gettext.'%')->get()->toArray();
        // dd($data);
        return view('fontend/search/search_product',compact('data','dataCategory','dataBrand'));      
    }

    /**
     * Store a newly created resource in storage.
     */
    public function search_price(Request $request)
    {   
        $price =$_POST['priceChange'];
        
        $priceChange = explode(':',$price);
        // var_dump($priceChange);
        $dataPrice = ProductsModel::whereBetween('price',$priceChange)->get()->toArray();
        // var_dump($dataPrice);
        $response = ['success' => $dataPrice];
        return response()->json($response);
    }
    /**
     * Display the specified resource.
     */
    public function get_search(Request $request)
    {
       
        $dataBrand = BrandModel::all()->toArray();
        $dataCategory = CategoryModel::all()->toArray();
        $price = $request->price;
        $name = $request->name;
        $priceInput = explode('-',$price);
        $category = $request->category;
        $brand = $request->brand;

        $dataAll = ProductsModel::query();

        if (!empty($name)) {
            $dataAll->where('name','like','%'.$name.'%');
        }
        if (!empty($price)) {
             $dataAll->whereBetween('price',$priceInput);
        }
        if (!empty($category)) {
             $dataAll->where('id_category',$category);
        }
        if (!empty($brand)) {
             $dataAll->where('id_brand',$brand);
        }
        $xx = $dataAll->get()->toArray();
        return view('fontend/search/search_product', compact('xx','dataCategory','dataBrand'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
