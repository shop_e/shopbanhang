<?php

namespace App\Http\Controllers\MemberController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserModel\ProductsModel;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('fontend/cart/cart');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function addCart(Request $request){
        $id = $_POST['id'];
        // echo $id;
        $dataProduct = ProductsModel::where('id_product',$id)->get()->toArray();
        $dataProduct = $dataProduct[0];
        $img = json_decode($dataProduct['image']);
        // dd($img[0]);
        $array = array(
            'id_product'=>$dataProduct['id_product'],
            'name'=>$dataProduct['name'],
            'price'=>$dataProduct['price'],
            'img'=>$img[0],
            'qty'=>1,
        );
        $check =true;
        $sumqty = 1;
        $sumtotal =0;
        if (session()->has('cart')) {
            $getSS = session()->get('cart');
            // var_dump($getSS);
            foreach ($getSS as $key => $value) {

                if ($value['id_product']==$id) {
                     // echo $value['id_product']."----";
                    $getSS[$key]['qty']+=1;
                    session()->put('cart',$getSS);
                    $check=false;
                }
                $sumqty+=$value['qty'];
            }
        }
        if ($check) {
            session()->push('cart', $array);
         
        };

        if (session()->has('cart')) {
                $cartItems = session()->get('cart');
                foreach ($cartItems as $item) {
                    $sumtotal += ($item['qty'] * $item['price']);
                }
            }
        session()->put('total',$sumtotal);   
        $response = ['success111' => $sumqty];
        return response()->json($response);
        var_dump(session()->get('total'));
        // echo '<pre>';   
        // var_dump(session()->get('cart'));
        // session()->flush(); 
        // echo $sumqty;
 
        

    } 
   
    public function upCart(Request $request)
    {
       $idUp = $_POST['idUp'];
       // echo $idUp;
       //  $check = true;
       // if (session()->has('cart')) {
        $sumtotal = 0;
        $getSS = session()->get('cart');
            foreach ($getSS as $key => $value) {
                // echo $value['id_product']."----";
               if ($value['id_product']==$idUp) {
                    // echo $value['id_product']."----";
                   $getSS[$key]['qty']+=1;
                   session()->put('cart',$getSS);
                   // $check = false;
               }
           }
           if (session()->has('cart')) {
                $cartItems = session()->get('cart');

                foreach ($cartItems as $item) {
                    $sumtotal +=($item['qty'] * $item['price']);
                }
            }        
        session()->put('total',$sumtotal);   
        $response = ['total'=>$sumtotal];
        return response()->json($response);
       echo '<pre>';
       var_dump($getSS);
    }

    /**
     * Store a newly created resource in storage.
     */
     public function Downcart(Request $request)
        {
           $idDown = $_POST['idDown'];
            $sumtotal = 0;
           if (session()->has('cart')) {
            $getSS = session()->get('cart');
                foreach ($getSS as $key => $value) {
                   if ($getSS[$key]['id_product']==$idDown) {
                        $getSS[$key]['qty']--;
                        session()->put('cart',$getSS);
                        break;
                   }
               }    
               if($getSS[$key]['qty']<=0){
                    unset($getSS[$key]);
               }
                session()->put('cart',$getSS);    
               }

               if (session()->has('cart')) {
                    $cartItems = session()->get('cart');

                    foreach ($cartItems as $item) {
                        $sumtotal +=($item['qty'] * $item['price']);
                    }
                } 
            session()->put('total',$sumtotal);        
            $response = ['total'=>$sumtotal];
            return response()->json($response); 
           echo '<pre>';
           var_dump(session()->get('cart'));
        }
    public function deleteCart(Request $request)
    {
        $idRemove = $_POST['idRemove'];
        $sumtotal = 0;
        if (session()->has('cart')) {
            $getSS = session()->get('cart');
            foreach($getSS as $key=>$value){
                if ($value['id_product']==$idRemove) {
                    unset($getSS[$key]);
                    break;
                }
            }
                session()->put('cart',$getSS);
        }

        if (session()->has('cart')) {
                    $cartItems = session()->get('cart');

                    foreach ($cartItems as $item) {
                        $sumtotal += ($item['qty'] * $item['price']);
                    }
                }
            session()->put('total',$sumtotal);               
            $response = ['total'=>$sumtotal];
            return response()->json($response); 
           echo '<pre>';
           var_dump(session()->get('cart'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
         // $sumproduct = 0; 

        // session()->flush(); 
        // echo $sumproduct;
        // echo "<pre>";
        // var_dump(session()->get('cart'));
            //Hàm trả về sucess bên ajax theo têm success111
       // $response = ['success111' => $sumproduct];
       //  return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
