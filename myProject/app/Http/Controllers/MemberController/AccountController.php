<?php

namespace App\Http\Controllers\MemberController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserModel\ProductsModel;
use App\Models\AdminModel\CategoryModel;
use App\Models\AdminModel\BrandModel;
use Illuminate\Support\Facades\Auth;
use Image;
class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
      */
        public function __construct()
        {
            $this->middleware('auth');
        }
        public function index()
        {   
            $dataUser = Auth::user()->toArray();

            return view('fontend/account/account',compact('dataUser'));
        }

    /**
     * Show the form for creating a new resource.
     */
    public function trangadd()
    {
        $dataCategory = CategoryModel::all()->toArray();
        $dataBrand= BrandModel::all()->toArray();
        // dd($dataCategory);
        return view('fontend/account/add_product',compact('dataCategory','dataBrand'));
    }
     public function addproduct(Request $request)
    {   
        if ($request->hasfile('image')) {
            $data = [];
            foreach ($request->file('image') as $image) {
                $name = $image->getClientOriginalName();
                $name_2 = "85x84".$image->getClientOriginalName();
                $name_3 = "329x380".$image->getClientOriginalName();
                    //Lưu ảnh
                $path = public_path('fontend/images/product-details/' .$name);
                $path_2 = public_path('fontend/images/product-details/' .$name_2);
                $path_3 = public_path('fontend/images/product-details/' .$name_3);

                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(85, 84)->save($path_2);
                Image::make($image->getRealPath())->resize(392, 380)->save($path_3);
                $data[] = $name;
            }
            if (count($data)>3) {
                return redirect()->back()->with('success', __('Chi chon duoc 3 hinh'));
            }
            else{

                // dd($data);
                $id_user=Auth::id();
                ProductsModel::insert([
                    'id_user'=>$id_user,
                    'name'=>$request->name,
                    'price'=>$request->price,
                    'id_category'=>$request->input('category'),
                    'id_brand'=>$request->input('brand'),
                    'status'=>$request->input('status'),
                    'sale'=>$request->phamtram,
                    'company'=>$request->company,
                    'image'=>json_encode($data),
                    'detail'=>$request->detail
                ]);
                 return redirect('account/my_product')->with('success', __('Add product success.'));
            }

            }else{
                return redirect()->back()->with('success', __('Chi chon duoc 3 hinh'));
            }

        
    }

    /**
     * Store a newly created resource in storage.
     */
    /**
     * Display the specified resource.
     */
    public function showproduct()
    {   $id_user=Auth::id();
        $dataProduct = ProductsModel::where('id_user',$id_user)->get()->toArray();
       
        return view('fontend/account/my_product', compact('dataProduct'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit_product(string $id)
    {   $dataCategory = CategoryModel::all()->toArray();
        $dataEdit = ProductsModel::where('id_product', $id)->get()->toArray();
        $dataEdit = $dataEdit[0];

        return view('fontend/account/edit_product' ,compact('dataEdit','dataCategory'));
    }
    public function update_product(Request $request, string $id)
    {   
        $product = ProductsModel::find($id);
         //Data tu request
        $data = $request->all(); 
        $imagesToDelete = $request->input('imagesToDelete', []);

        //Data Cu
       $edit_product = ProductsModel::where('id_product',$id)->get();
       $oldImages = json_decode($edit_product[0]->image, true);
       foreach ($imagesToDelete as $image) {
           if (in_array($image, $oldImages)) {
            $key = array_search($image,$oldImages);
            unset($oldImages[$key]);
            }
        }

        if ($request->hasfile('image')) {
            $dataimg = [];
            foreach ($request->file('image') as $image) {
                $name = $image->getClientOriginalName();
                $name_2 = "85x84".$image->getClientOriginalName();
                $name_3 = "329x380".$image->getClientOriginalName();
                    //Lưu ảnh
                $path = public_path('fontend/images/product-details/' .$name);
                $path_2 = public_path('fontend/images/product-details/' .$name_2);
                $path_3 = public_path('fontend/images/product-details/' .$name_3);

                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(85, 84)->save($path_2);
                Image::make($image->getRealPath())->resize(392, 380)->save($path_3);

                $dataimg[] = $name;
            }
                if (count($oldImages) + count($dataimg)>3) {
                    return redirect()->back()->with('error', 'Tổng số hình ảnh không được vượt quá 3.');
                }else{
                    $product->image = json_encode(array_merge($dataimg,$oldImages));
                }
        }else{
            if (count($oldImages)<1) {
                return redirect()->back()->with('error', 'K duoc xoa het hinh');
            }else{
                $product->image = json_encode($oldImages);
            }
            
        }
        ProductsModel::where('id_product',$id)->update([
                    'name'=>$request->name,
                    'price'=>$request->price,
                    'id_category'=>$request->input('category'),
                    'id_brand'=>$request->input('brand'),
                    'status'=>$request->input('status'),
                    'sale'=>$request->phamtram,
                    'company'=>$request->company,
                    'image'=>$product->image,
                    'detail'=>$request->detail
        ]);
            return redirect('account/my_product')->with('success', __('Update product success.'));
       }
    public function update(Request $request)
    {
         $user_id=Auth::id();
         $user = user::findOrFail($user_id);
         // dd($user);
         $data =$request->all();
         $file = $request->avatar;
         dd($data);
         if (!empty($file)) {
             $data['avatar']=$file->getClientOriginalName();
         }
         if ($data['password']) {
             $data['password']= bcrypt($data['password']);
         }else{
            $data['password']=$user->password;
         }
         if ($user->update($data)) {
             if (!empty($file)) {   
                 $file->move('avatar',$file->getClientOriginalName());
             }
             return redirect()->back()->with('success', __('Update product success.'));
         }else {
            return redirect()->back()->withErrors('Update product error.');
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        ProductsModel::where('id_product',$id)->delete();
        return redirect('account/my_product')->with('success', __('Delete product success.'));

    }
}
