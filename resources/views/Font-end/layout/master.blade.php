<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Coron - Fashion eCommerce Bootstrap4 Template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('font_end\assets\img\favicon.png')}}">
		
		<!-- all css here -->
        <link rel="stylesheet" href="{{asset('font_end\assets\css\bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('font_end\assets\css\plugin.css')}}">
        <link rel="stylesheet" href="{{asset('font_end\assets\css\bundle.css')}}">
        <link rel="stylesheet" href="{{asset('font_end\assets\css\style.css')}}">
        <link rel="stylesheet" href="{{asset('font_end\assets\css\responsive.css')}}">
        <script src="{{asset('font_end\assets\js\vendor\modernizr-2.8.3.min.js')}}"></script>
    </head>
    <body>
        <div class="pos_page">
            <div class="container">
               <!--pos page inner-->
                <div class="pos_page_inner">
                    @include('Font-end.layout.header')

                    @yield('content')
                </div>
           </div> 
        </div> 
        @include('Font-end.layout.footer')
        <script src="{{asset('font_end\assets\js\vendor\jquery-1.12.0.min.js')}}"></script>
        <script src="{{asset('font_end\assets\js\popper.js')}}"></script>
        <script src="{{asset('font_end\assets\js\bootstrap.min.js')}}"></script>
        <script src="{{asset('font_end\assets\js\ajax-mail.js')}}"></script>
        <script src="{{asset('font_end\assets\js\plugins.js')}}"></script>
        <script src="{{asset('font_end\assets\js\main.js')}}"></script>
    </body>
</html>