@extends('Back-end.layout.master')
@section('content')
<div class="container-fluid pt-4 px-4">
	<div class="row g-4">
		<div class="col-12">
		        <div class="bg-secondary rounded h-100 p-4">
	            <h6 class="mb-4">List User</h6>
	            <div class="table-responsive">
	                <table class="table">
	                    <thead>
	                        <tr>
	                            <th scope="col">#</th>
	                            <th scope="col">Name</th>
	                            <th scope="col">Email</th>
	                            <th scope="col">Phone</th>
	                            <th scope="col">Role</th>
	                            <th scope="col">Time Create</th>
								<th scope="col">Action</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php foreach ($dataUser as $value): ?>
	                    	<tr>
	                            <th scope="row">1</th>
	                            <td>{{$value['name']}}</td>
	                            <td>{{$value['email']}}</td>
	                            <td>{{$value['phone']}}</td>
	                            <td>{{$value['id_role']}}</td>
	                            <td>{{$value['created_at']}}</td>
	                            <td><a href="">Edit |</a><a href="">| Delete</a></td>
	                        </tr>
	                    	<?php endforeach ?>
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
</div>
@endsection