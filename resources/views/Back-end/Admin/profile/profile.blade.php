@extends('Back-end.layout.master')
@section('content')
	<!-- Form Start -->
<div class="container-fluid pt-4 px-4">
	<div class="row g-4">
		<div class="col-sm-12 col-xl-6">
	        <div class="bg-secondary rounded h-100 p-4">
	            <h6 class="mb-4">Information User</h6>
	            <form action="{{route('profile.admin')}}" method="POST" enctype="multipart/form-data">
	            	@csrf
	                <div class="mb-3">
	                    <label for="exampleInputEmail1" class="form-label">Name</label>
	                    <input name="name" type="name" class="form-control" id="exampleInputEmail1" value="{{$dataProfile['name']}}">
	                </div>
	                <div class="mb-3">
	                    <label for="exampleInputEmail1" class="form-label">Email address</label>
	                    <input name="email" type="email" class="form-control" id="exampleInputEmail1"
	                        aria-describedby="emailHelp" value="{{$dataProfile['email']}}">
	                    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.
	                    </div>
	                </div>
	                <div class="mb-3">
	                    <label for="exampleInputEmail1" class="form-label">Phone</label>
	                    <input name="phone" type="phone" class="form-control" id="exampleInputEmail1" value="{{$dataProfile['phone']}}">
	                </div>
	                <div class="mb-3">
	                    <label for="exampleInputEmail1" class="form-label">Avatar</label>
	                    <input type="file" class="form-control" id="exampleInputEmail1" name="avatar">
	                </div>
	                <div class="mb-3">
	                    <label for="exampleInputEmail1" class="form-label">Password</label>
	                    <input name="password" type="password" class="form-control" id="exampleInputEmail1" value="">
	                </div>
	                <button type="submit" class="btn btn-primary">Update</button>
	            </form>
	             @if(session('success'))
	                   <h5 style="color: red; width: 100%; text-align:center;"> {{ session('success') }}</h5>
	            @endif
	             @if(session('error'))
	                <div class="alert alert-success">
	                   <h5 style="color: red; width: 100%; text-align:center;"> {{ session('error') }}</h5>
	                </div>
	            @endif
	        </div>
	    </div>
	</div>
</div>
<!-- Form End -->
@endsection
