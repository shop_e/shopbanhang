<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {   
        $dataProfile = Auth::user()->toArray();
        // dd($dataProfile);
        return view('Back-end/Admin/profile/profile',compact('dataProfile'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create_av()
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function update(Request $request)
    {
        $id_user = Auth::id();
        // dd($id_user);
        $user = user::findOrFail($id_user);
        $dataRequest = $request->all();
        $file = $request->avatar;
        if(!empty($file)){
            $dataRequest['avatar'] = $file->getClientOriginalName();
        };
        // dd($dataRequest['avatar']);

        if ($dataRequest['password']) {
            $dataRequest['password']=bcrypt($dataRequest['password']);
        }else{
            $dataRequest['password']=$user->password;
        };
        if ($user->update($dataRequest)) {
            if (!empty($file)) {
                $file->move('back-end\assets\img\avatar',$file->getClientOriginalName());
            }
                return redirect()->back()->with('success', __('Updata profile success'));
        }else{
                return redirect()->back()->with('error', __('Updata profile error'));

        }
    }
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
