<?php

use Illuminate\Support\Facades\Route;
//Admin
use App\Http\Controllers\AdminController\HomeAdminController;
use App\Http\Controllers\AdminController\DashboardController;
use App\Http\Controllers\AdminController\ProfileController;
use App\Http\Controllers\AdminController\ListUserController;
//Member
use App\Http\Controllers\Member\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });





Route::group(['prefix'=>'admin' ,'middleware' => ['auth']], function(){
	Route::get('/',[HomeAdminController::class,'index'])->name('admin.home');
		Route::group(['prefix'=>'dashboard'], function(){
			Route::get('/',[DashboardController::class,'index'])->name('dashboard.admin');
		});
		Route::group(['prefix'=>'profile'], function(){
			Route::get('/',[ProfileController::class,'index'])->name('profile.admin');
			Route::post('/update',[ProfileController::class,'update'])->name('profile.update.admin');
		});
		Route::group(['prefix'=>'listuser'],function(){
			Route::get('/',[ListUserController::class,'index'])->name('listuser.admin');
		});

});

Route::group(['prefix'=>'index', 'namespace'=>'Member'],function(){
	Route::get('/home',[HomeController::class,'index'])->name('index.home');

});


Auth::routes();
Route::get('/index', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

